﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        string output
        {
            get
            {
                return textBoxOutput.Text;
            }

            set
            {
                textBoxOutput.Text = value;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        // SOURCE FILE
        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxSourceFile.Text = openFileDialog1.FileName;
                textBoxFilename.Text = System.IO.Path.GetFileNameWithoutExtension(openFileDialog1.FileName) + ".mp4";
            }
        }

        // DESTINATION FILE
        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxDestinationFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        void start(string commandPath, string commandParams)
        {
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();

                process.StartInfo.UseShellExecute = false;

                process.EnableRaisingEvents = true;

                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                process.StartInfo.CreateNoWindow = true;

                process.StartInfo.FileName = commandPath;
                process.StartInfo.Arguments = commandParams;

                process.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(DataReceived);
                process.ErrorDataReceived += new System.Diagnostics.DataReceivedEventHandler(ErrorReceived);

                process.Exited += Process_Exited;

                output = string.Format("{0} {1}\n", System.IO.Path.GetFileName(commandPath), commandParams);

                Properties.Settings.Default.last_source_file = textBoxSourceFile.Text;
                Properties.Settings.Default.last_output_dir = textBoxDestinationFolder.Text;
                Properties.Settings.Default.last_output_filename = textBoxFilename.Text;
                Properties.Settings.Default.last_commandline = textBoxCommandLine.Text;

                saveCommandsList();

                Properties.Settings.Default.Save();

                buttonStart.Enabled = false;
                if (process.Start())
                {
                    backgroundWorker1.RunWorkerAsync(process);
                }                
                else
                {
                    buttonStart.Enabled = true;
                }
            }
            catch (System.Exception e)
            {
                buttonStart.Enabled = true;
            }
        }

        private void Process_Exited(object sender, System.EventArgs e)
        {
            
        }

        void DataReceived(object sender, System.Diagnostics.DataReceivedEventArgs eventArgs)
        {
            if (!string.IsNullOrEmpty(eventArgs.Data))
            {
                AddText(eventArgs.Data + "\n");
            }
        }

        void ErrorReceived(object sender, System.Diagnostics.DataReceivedEventArgs eventArgs)
        {
            AddText("\n" + eventArgs.Data + "\n");
        }

        public string LastInputFileName;
        public string LastOutputFileName;

        // START
        private void button3_Click(object sender, EventArgs e)
        {
            string appPath = Properties.Settings.Default.app_path;

            LastInputFileName = textBoxSourceFile.Text;
            LastOutputFileName = textBoxDestinationFolder.Text.TrimEnd(new char[] { '\\' }) + "\\" + textBoxFilename.Text;

            string appParams = textBoxCommandLine.Text.Replace("{0}", textBoxSourceFile.Text).Replace("{1}", LastOutputFileName);

            if (System.IO.File.Exists(LastOutputFileName))
            {
                if (MessageBox.Show("Delete the file " + LastOutputFileName + "?", "File already exists", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    System.IO.File.Delete(LastOutputFileName);
                }
                else
                {
                    return;
                }
            }

            start(appPath, appParams);
        }

        // This delegate enables asynchronous calls for setting
        // the text property on a TextBox control.
        delegate void SetTextCallback(string text);
        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.textBoxOutput.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textBoxOutput.Text = text;
            }
        }
        private void AddText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.textBoxOutput.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(AddText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textBoxOutput.Text += text;
            }
        }
        delegate void SetEnabledCallback(bool enabled);

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Diagnostics.Process _process = e.Argument as System.Diagnostics.Process;
            {
                _process.BeginErrorReadLine();
                _process.BeginOutputReadLine();
                _process.WaitForExit();
                _process.Close();  
                e.Result = _process;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonStart.Enabled = true;

            System.Diagnostics.Process _process = e.Result as System.Diagnostics.Process;
            if (_process != null)
            {
                MessageBox.Show("File converted!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);                
            }
            else
            {
                MessageBox.Show("Error converting file!", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBoxSourceFile.Text = Properties.Settings.Default.last_source_file;
            textBoxDestinationFolder.Text = Properties.Settings.Default.last_output_dir;
            textBoxFilename.Text = Properties.Settings.Default.last_output_filename;
            textBoxCommandLine.Text = Properties.Settings.Default.last_commandline;

            if (string.IsNullOrEmpty(textBoxCommandLine.Text))
            {
                textBoxCommandLine.Text  = "-i \"{0}\" -pix_fmt yuv420p -nostdin \"{1}\"";
                Properties.Settings.Default.last_commandline = textBoxCommandLine.Text;
                Properties.Settings.Default.Save();
            }

            if (System.IO.File.Exists(Properties.Settings.Default.app_path) == false)
            {
                if (openFileDialog2.ShowDialog() == DialogResult.OK)
                {
                    Properties.Settings.Default.app_path = openFileDialog2.FileName;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    return;
                }
            }

            loadCommandsList();
        }

        #region LIST

        // ADD
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            DialogCommand cmd = new DialogCommand();
            if (cmd.ShowDialog() == DialogResult.OK)
            {
                listBox1.Items.Add(cmd.Command);
                listBox1.SelectedItem = cmd.Command;
            }
        }

        // MODIFY
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                DialogCommand cmd = new DialogCommand(listBox1.SelectedItem as string);
                if (cmd.ShowDialog() == DialogResult.OK)
                {
                    listBox1.SelectedItem = cmd.Command;
                }
            }
        }

        void saveCommandsList()
        {
            string f = "";
            foreach (string s in listBox1.Items)
            {
                f += s + "\n";
            }
            Properties.Settings.Default.last_commands_list = f;
            Properties.Settings.Default.Save();
        }

        void loadCommandsList()
        {
            string[] parts = Properties.Settings.Default.last_commands_list.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            listBox1.Items.Clear();
            listBox1.Items.AddRange(parts);
        }

        void executeCommandFromList(string command)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            {
                dictionary["{output-path}"] = textBoxOutput.Text;

                dictionary["{0}"] = LastInputFileName;
                dictionary["{0:filename}"] = System.IO.Path.GetFileNameWithoutExtension(LastInputFileName);
                dictionary["{0:extension}"] = System.IO.Path.GetExtension(LastInputFileName);
                dictionary["{0:extension}"] = System.IO.Path.GetExtension(LastInputFileName);

                dictionary["{1}"] = LastOutputFileName;
            }
        }

        #endregion        
    }
}
