﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class DialogCommand : Form
    {
        public string Command
        {
            get
            {
                return textBoxSourceFile.Text;
            }

            set
            {
                textBoxSourceFile.Text = value;
            }
        }

        public DialogCommand()
        {
            InitializeComponent();
        }

        public DialogCommand(string cmdParam)
        {
            InitializeComponent();

            Command = cmdParam;
        }
    }
}
