﻿namespace WindowsFormsApplication1
{
    partial class ConvertOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "yuv420p",
            "yuyv422",
            "rgb24",
            "bgr24",
            "yuv422p",
            "yuv444p",
            "yuv410p",
            "yuv411p",
            "gray",
            "monow",
            "monob",
            "pal8",
            "yuvj420p",
            "yuvj422p",
            "yuvj444p",
            "xvmcmc",
            "xvmcidct",
            "uyvy422",
            "uyyvyy411",
            "bgr8",
            "bgr4",
            "bgr4_byte",
            "rgb8",
            "rgb4",
            "rgb4_byte",
            "nv12",
            "nv21",
            "argb",
            "rgba",
            "abgr",
            "bgra",
            "gray16be",
            "gray16le",
            "yuv440p",
            "yuvj440p",
            "yuva420p",
            "vdpau_h264",
            "vdpau_mpeg1",
            "vdpau_mpeg2",
            "vdpau_wmv3",
            "vdpau_vc1",
            "rgb48be",
            "rgb48le",
            "rgb565be",
            "rgb565le",
            "rgb555be",
            "rgb555le",
            "bgr565be",
            "bgr565le",
            "bgr555be",
            "bgr555le",
            "vaapi_moco",
            "vaapi_idct",
            "vaapi_vld",
            "yuv420p16le",
            "yuv420p16be",
            "yuv422p16le",
            "yuv422p16be",
            "yuv444p16le",
            "yuv444p16be",
            "vdpau_mpeg4",
            "dxva2_vld",
            "rgb444le",
            "rgb444be",
            "bgr444le",
            "bgr444be",
            "ya8",
            "bgr48be",
            "bgr48le",
            "yuv420p9be",
            "yuv420p9le",
            "yuv420p10be",
            "yuv420p10le",
            "yuv422p10be",
            "yuv422p10le",
            "yuv444p9be",
            "yuv444p9le",
            "yuv444p10be",
            "yuv444p10le",
            "yuv422p9be",
            "yuv422p9le",
            "vda_vld",
            "gbrp",
            "gbrp9be",
            "gbrp9le",
            "gbrp10be",
            "gbrp10le",
            "gbrp16be",
            "gbrp16le",
            "yuva422p",
            "yuva444p",
            "yuva420p9be",
            "yuva420p9le",
            "yuva422p9be",
            "yuva422p9le",
            "yuva444p9be",
            "yuva444p9le",
            "yuva420p10be",
            "yuva420p10le",
            "yuva422p10be",
            "yuva422p10le",
            "yuva444p10be",
            "yuva444p10le",
            "yuva420p16be",
            "yuva420p16le",
            "yuva422p16be",
            "yuva422p16le",
            "yuva444p16be",
            "yuva444p16le",
            "vdpau",
            "xyz12le",
            "xyz12be",
            "nv16",
            "nv20le",
            "nv20be",
            "rgba64be",
            "rgba64le",
            "bgra64be",
            "bgra64le",
            "yvyu422",
            "vda",
            "ya16be",
            "ya16le",
            "gbrap",
            "gbrap16be",
            "gbrap16le",
            "qsv",
            "mmal",
            "d3d11va_vld",
            "cuda",
            "0rgb",
            "rgb0",
            "0bgr",
            "bgr0",
            "yuv420p12be",
            "yuv420p12le",
            "yuv420p14be",
            "yuv420p14le",
            "yuv422p12be",
            "yuv422p12le",
            "yuv422p14be",
            "yuv422p14le",
            "yuv444p12be",
            "yuv444p12le",
            "yuv444p14be",
            "yuv444p14le",
            "gbrp12be",
            "gbrp12le",
            "gbrp14be",
            "gbrp14le",
            "yuvj411p",
            "bayer_bggr8",
            "bayer_rggb8",
            "bayer_gbrg8",
            "bayer_grbg8",
            "bayer_bggr16le",
            "bayer_bggr16be",
            "bayer_rggb16le",
            "bayer_rggb16be",
            "bayer_gbrg16le",
            "bayer_gbrg16be",
            "bayer_grbg16le",
            "bayer_grbg16be",
            "yuv440p10le",
            "yuv440p10be",
            "yuv440p12le",
            "yuv440p12be",
            "ayuv64le",
            "ayuv64be",
            "videotoolbox_vld",
            "p010le",
            "p010be",
            "gbrap12be",
            "gbrap12le"});
            this.comboBox1.Location = new System.Drawing.Point(92, 11);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(186, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pixel Format";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(291, 14);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(65, 17);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Enabled";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(291, 37);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(65, 17);
            this.checkBox2.TabIndex = 4;
            this.checkBox2.Text = "Enabled";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Output Size";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(92, 34);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(92, 20);
            this.numericUpDown1.TabIndex = 5;
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(190, 34);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(88, 20);
            this.numericUpDown2.TabIndex = 6;
            // 
            // ConvertOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 166);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Name = "ConvertOptions";
            this.Text = "ConvertOptions";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
    }
}